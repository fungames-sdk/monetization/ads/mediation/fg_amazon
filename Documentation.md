# Amazon with Applovin Max

## Integration Steps

1) **"Install"** or **"Upload"** FG Amazon plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import Amazon SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG Amazon module from the Integration Manager window, you will find the last compatible version of Amazon SDK in the _Assets > FunGames_Externals > Mediation_ folder. Double click on the .unitypackage file to install it.

Once Amazon SDK is installed, you will also need to Install the adapter for ApplovinMax. You can do it through the Amazon SDK Manager (_Unity toolbar > Amazon > Manage SDKs..._)

![](_source/amazon_maxAdapter.png)

## Account and Settings

Ask your Publisher to create your app on the APS account and to provide you the credentials for your app.

Once done, add your app Keys and Ad Ids to the **FG Amazon Settings** (_Assets > Resources > FunGames > FGAmazonSettings_).

## Build specifications

### Android

```csharp
<activity android:name="com.amazon.device.ads.DTBInterstitialActivity"/>
<activity android:name="com.amazon.device.ads.DTBAdActivity"/>`
```

For Amazon integration, you will also need to add these lines under the `<application>` section of your manifest file:

### IOS

When building your project for iOS, you might need to add DTBIOSSDK.xcframework manually to your Xcode Workspace. To do so, select Unity-iPhone target in XCode, open _General_ section, and add it using the "+" button in _Frameworks, Libraries and Embedded Content_.

![](_source/amazon_iOSbuild.png)

You will also have to disable Bitcode in the _Build Settings_ of your XCode project.

![](_source/amazon_disableBitcode.png)

# TROUBLESHOOTING

## Build error : No visible interface for 'DTBAdResponse'

When building on iOS, you might face this _"No visible @interface for 'DTBAdResponse' declares the selector 'keywordsForMopub'"_ error if Amazon SDK / adapter have not been installed properly.

![](_source/amazon_troubleshoot_noVisibleInterface.png)

To solve this issue, make sure you have properly installed/upgraded Amazon SDK and Applovin Max adapter (see "Install External Plugin" section).

If everything looks good, but you still face the issue, then we suggest to retry the Amazon install from scratch (delete _Assets/Amazon_ folder and import it again). You will need to install Amazon adapter again (_Unity toolbar > Amazon > Manage SDKs..._).
